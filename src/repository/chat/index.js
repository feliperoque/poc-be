import { PubSub } from 'apollo-server-express';
import uuid from 'uuid/v1';
import nats from 'src/nats';
import redis from 'src/redis';
import { withCancel} from 'api';

const pubsub = new PubSub();
const subscriptions = {};

export async function fetchChat(chatId) {
  const chat = await redis.retrieveChat(chatId);
  return chat || { id: chatId, messages: [] };
}

export async function sendMessageToChat(user, chatId, content) {
  console.log(`Sending Message ${content} from ${user} on #${chatId}`);
  const message = buildMessage(user, content);
  publishMessage(chatId, message);
  return { success: true };
}

export function subscribeToChat(user, chatId) {
  const subId = nats.subscribeToChat(user, chatId, message => receiveMessage(user, chatId, message));
  const userSubscriptions = subscriptions[`${user}`] || {};
  userSubscriptions[`${chatId}`] = subId;
  setTimeout(() => notifyUserJoining(user, chatId), 100);
  return withCancel(pubsub.asyncIterator(chatId), () => {
    notifyUserLeaving(user, chatId);
    return unsubscribeFromChat(user, chatId);
  });
}

function unsubscribeFromChat(user, chatId) {
  const userSubscriptions = subscriptions[`${user}`] || {};
  const subId = userSubscriptions[`${chatId}`] || null;
  return nats.unsubscribeFromChat(user, chatId, subId);
}

async function publishMessage(chatId, message) {
  const chat = await fetchChat(chatId);
  chat.messages.push(message);
  pubsub.publish(chatId, { chat });
  nats.sendMessageToChat(chatId, message);
  redis.storeMessage(chatId, message);
}

async function receiveMessage(user, chatId, message) {
  if (message.from === user) return;
  console.log(`Receiving Message ${message.content} from ${message.from} on #${chatId}`);
  const chat = await fetchChat(chatId);
  const repeatedMessage = chat.messages.find(current => current.id === message.id);
  if (!repeatedMessage) chat.messages.push(message);
  pubsub.publish(chatId, { chat });
}

function notifyUserJoining(user, chatId) {
  const message = buildMessage('MYQ_SERVER', `${user} joined #${chatId}`);
  publishMessage(chatId, message);
}

function notifyUserLeaving(user, chatId) {
  const message = buildMessage('MYQ_SERVER', `${user} left #${chatId}`);
  publishMessage(chatId, message);
}

function buildMessage(user, content) {
  return {
    id: uuid(),
    from: user,
    content: content,
    date: Date.now(),
  };
}