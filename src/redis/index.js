import redis from 'redis';
const client = redis.createClient('6379', '192.168.99.100');

async function storeMessage(chatId, message) {
  const chat = await retrieveChat(chatId) || { id: chatId, messages: [] };
  chat.messages.push(message);
  client.set(chatId, JSON.stringify(chat));
  return Promise.resolve();
}

function retrieveChat(chatId) {
  return new Promise(resolve => {
    client.get(chatId, (error, result) => {
      resolve(JSON.parse(result));
    });
  });
}

export default {
  storeMessage,
  retrieveChat,
}