import nats from "nats";
const client = nats.connect({ json: true, servers: ['nats://192.168.99.100:4222'] });

function sendMessageToChat(chatId, message) {
  return client.publish(`chat_message_${chatId}`, message);
}

function subscribeToChat(user, chatId, onMessage) {
  console.log(`${user} has joined #${chatId}`);
  return client.subscribe(`chat_message_${chatId}`, message => onMessage(message));
}

function unsubscribeFromChat(user, chatId, subId) {
  console.log(`${user} has left #${chatId}`);
  return client.unsubscribe(subId);
}

export default {
  sendMessageToChat,
  subscribeToChat,
  unsubscribeFromChat,
}