
import { ApolloServer, gql } from 'apollo-server-express';
import { importSchema } from 'graphql-import';
import { makeExecutableSchema } from 'graphql-tools';
import resolver from './resolver';

const importedSchema = importSchema('./src/api/schema.graphql');
const config = {
  typeDefs: gql`${importedSchema}`,
  resolvers: resolver,
  uploads: false,
};

export const withCancel = (asyncIterator, onCancel) => {
  const asyncReturn = asyncIterator.return;
  asyncIterator.return = () => {
    onCancel();
    return asyncReturn ? asyncReturn.call(asyncIterator) : Promise.resolve({ value: undefined, done: true });
  };
  return asyncIterator;
};

const server = new ApolloServer(config);
export const schema = makeExecutableSchema(config);

export default server;