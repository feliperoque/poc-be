import { fetchChat, sendMessageToChat, subscribeToChat } from 'repository/chat';

const resolvers = {

  Query: {

    chat: (parent, args, context) => fetchChat(args.chatId),

  },

  Mutation: {

    sendMessage: (parent, args, context) => sendMessageToChat(args.user, args.chatId, args.content),

  },

  Subscription: {

    chat: {
      subscribe: (parent, args, context) => subscribeToChat(args.user, args.id),
    },

  },

};

export default resolvers;